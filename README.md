# nMigen Examples (To be used with Libre-SOC flow)

See the Libre-SOC nMigen [tutorial page](https://libre-soc.org/docs/learning_nmigen/)

## Counter example

Example modified from [original in nMigen repo](https://gitlab.com/nmigen/nmigen/blob/master/docs/start.rst).

Includes gtkw nmutils lib for generating the .gtkw trace file.

To run:

    python3 up_counter.py
